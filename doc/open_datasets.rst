.. _open_datasets:

=============
Open datasets
=============

.. _holz_data0:

Higher order Laue zone analysis
-------------------------------

* Zenodo DOI: `10.5281/zenodo.3476746 <https://dx.doi.org/10.5281/zenodo.3476746>`_
* Medipix3 dataset download link: https://zenodo.org/record/3476746/files/m004_LSMO_LFO_STO_medipix.hdf5?download=1
* Journal publication: `Three-dimensional subnanoscale imaging of unit cell doubling due to octahedral tilting and cation modulation in strained perovskite thin films <https://doi.org/10.1103/PhysRevMaterials.3.063605>`_
* Jupyter Notebook using this dataset: `Basic structural analysis of 4D-STEM data <https://gitlab.com/pixstem/pixstem_demos/-/raw/release/4d_stem_basic_structural_analysis.ipynb?inline=false>`_


.. _feal_data:

Ferromagnetic STEM-DPC analysis
-------------------------------

* Zenodo DOI: `10.5281/zenodo.3466591 <https://doi.org/10.5281/zenodo.3466591>`_
* Journal publication: `Strain Anisotropy and Magnetic Domains in Embedded Nanomagnets <https://doi.org/10.1002/smll.201904738>`_
* Jupyter Notebook using one of the datasets: `Analysing magnetic materials using STEM-DPC <https://gitlab.com/pixstem/pixstem_demos/-/raw/release/4d_stem_analysis_magnetic_samples.ipynb?inline=false>`_
